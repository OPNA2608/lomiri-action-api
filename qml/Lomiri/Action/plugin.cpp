/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plugin.h"

#include <QtQml>

#include "qml-action.h"
#include "qml-preview-action.h"
#include "qml-context.h"
#include "qml-manager.h"

#include <lomiri/action/PreviewParameter>
#include <lomiri/action/PreviewRangeParameter>
#include <lomiri/action/MenuItem>

void
LomiriActionQmlPlugin::registerTypes(const char *uri)
{
    // @uri Lomiri.Action

    qmlRegisterType<lomiri::action::Action>                     ();
    qmlRegisterType<lomiri::action::qml::Action>                (uri, 1, 0, "Action");
    qmlRegisterType<lomiri::action::qml::Action>                (uri, 1, 1, "Action");
    qmlRegisterType<lomiri::action::PreviewAction>              ();
    qmlRegisterType<lomiri::action::qml::PreviewAction>         (uri, 1, 0, "PreviewAction");
    qmlRegisterType<lomiri::action::qml::PreviewAction>         (uri, 1, 1, "PreviewAction");

    qmlRegisterType<lomiri::action::PreviewParameter>      ();
    qmlRegisterType<lomiri::action::PreviewRangeParameter> (uri, 1, 0, "PreviewRangeParameter");
    qmlRegisterType<lomiri::action::PreviewRangeParameter> (uri, 1, 1, "PreviewRangeParameter");

    // Don't provide menu item just yet.
    //qmlRegisterType<lomiri::action::MenuItem> (uri, 1, 0, "MenuItem");

    qmlRegisterType<lomiri::action::ActionContext>      ();
    qmlRegisterType<lomiri::action::qml::ActionContext> (uri, 1, 0, "ActionContext");
    qmlRegisterType<lomiri::action::qml::ActionContext> (uri, 1, 1, "ActionContext");
    qmlRegisterType<lomiri::action::ActionManager> (uri, 1, 0, "");
    qmlRegisterType<lomiri::action::ActionManager, 1> (uri, 1, 1, "");
    qmlRegisterType<lomiri::action::qml::ActionManager> (uri, 1, 0, "ActionManager");
    qmlRegisterType<lomiri::action::qml::ActionManager, 1> (uri, 1, 1, "ActionManager");

}

void
LomiriActionQmlPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
  Q_UNUSED(uri);
  Q_UNUSED(engine);
}


