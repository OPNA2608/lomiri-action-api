/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_MANAGER
#define LOMIRI_ACTION_MANAGER

namespace lomiri {
namespace action {
    class ActionManager;
    class ActionContext;
    class Action;
}
}

#include <QObject>
#include <QScopedPointer>

class Q_DECL_EXPORT lomiri::action::ActionManager : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ActionManager)

    Q_PROPERTY(lomiri::action::ActionContext *globalContext
               READ globalContext)

public:

    explicit ActionManager(QObject *parent = 0);
    virtual ~ActionManager();

    Q_INVOKABLE void addAction(lomiri::action::Action *action);
    Q_INVOKABLE void removeAction(lomiri::action::Action *action);

    Q_INVOKABLE lomiri::action::ActionContext *globalContext();

    Q_INVOKABLE void addLocalContext(lomiri::action::ActionContext *context);

    Q_INVOKABLE void removeLocalContext(lomiri::action::ActionContext *context);
    QSet<ActionContext *> localContexts() const;

    QSet<Action *> actions() const;

signals:
    void localContextsChanged();
    void actionsChanged();

    Q_REVISION(1) void quit();

private:
        class Private;
        QScopedPointer<Private> d;
};
#endif
