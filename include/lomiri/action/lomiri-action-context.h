/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_CONTEXT
#define LOMIRI_ACTION_CONTEXT

namespace lomiri {
namespace action {
    class ActionContext;
    class Action;
}
}

#include <QObject>
#include <QScopedPointer>
#include <QSet>

class Q_DECL_EXPORT lomiri::action::ActionContext : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ActionContext)

    Q_PROPERTY(bool active
               READ active
               WRITE setActive
               NOTIFY activeChanged)

public:

    explicit ActionContext(QObject *parent = 0);
    virtual ~ActionContext();

    Q_INVOKABLE void addAction(lomiri::action::Action *action);
    Q_INVOKABLE void removeAction(lomiri::action::Action *action);

    bool active() const;
    void setActive(bool value);

    QSet<Action *> actions() const;

signals:
    void activeChanged(bool value);
    void actionsChanged();

private:
        class Private;
        QScopedPointer<Private> d;
};
#endif
